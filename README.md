# Creating a CSV delta file
Before starting, make sure your files are in the same directory as the main csvdelta.py script.  Both the orginal file and the changed file should have the same column structure

## Setup the environment
Run the command `pipenv shell` to make sure everything is ready

## Concatenate the csv files
1. Concat CSV files by running the following command:
`csvstack Output_set1.csv Output_set2.csv Output_set3.csv Output_set4.csv Output_set5.csv > concat.csv`

This may take a few mins and then you will have a combined file called `concat.csv` repeat this process with the changed filesets.  Make sure to rename the output file in the command to something different than `concat.csv` or your original file will be overwritten.  I opted for:
`csvstack Output_set1.csv Output_set2.csv Output_set3.csv Output_set4.csv Output_set5.csv > changed.csv`

## Convert the file so it can be read
1. Run this command on both files.  Windows add a bunch of speacial characters that need to be removed before the script will read it:
`dos2unix concat.csv` and `dos2unix changed.csv`. Again this may take some time if they are big files.

## Run the main script
Copy the following command making sure to set the right options and filenames

`python csvdelta.py --index="ID" concat.csv changed.csv`

### Breaking this down:
* `pipenv run python csvdelta.py` - executes the main script and makes sure that the required dependencies and software versions are used
* `index="ID"` - Selects the named column to match between the files.  This is used to work out if the row is new or has been changed
* `concat.csv changed.csv` - these are the files to be compared in the order *orginal-file.csv* *changed-file.csv*

When the script has run (*likely to take 6-10 minutes*) there will be a new file spit out called `delta.csv` in the same format as the original files.  If you run the script again, this file will be overwritten.
