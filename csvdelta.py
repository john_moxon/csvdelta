#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import mimetypes
import csv

from csvdiff import diff_files
from pathlib import Path
from datetime import datetime, timedelta

parser = argparse.ArgumentParser()
parser.add_argument("--index", "-index", action="store", default="id",
                        help="Column name in the csv to be used as the index")
parser.add_argument("original_file")
parser.add_argument("changed_file")

args = parser.parse_args()
csv_index = str(args.index)
original_file = args.original_file
changed_file = args.changed_file


# Check file exists and is correct mimetype
def check_file(fpath, type):
    return Path(str(fpath)).exists() and \
        mimetypes.guess_type(str(Path(str(fpath))))[0] == str(type)

# check the input file
if not check_file(original_file, 'text/csv'):
    print ("\nInput file is missing or incorrect type\n")
    print ("Usage: python regexr_tester.py input.csv regex.txt")
    print ("note - input.csv must be a single column csv of test data with no blank lines.\n\n")
    exit()

elif not check_file(changed_file, 'text/csv'):
    print ("\nRegex file is missing or incorrect type\n")
    print ("Usage: python regexr_tester.py input.csv regex.txt")
    print ("note - input.csv must be a single column csv of test data with no blank lines.\n\n")
    exit()

print("file checks complete\n")

print("starting benchmark timer\n")
start_time = datetime.now()
with open('delta.csv', mode='w') as delta_file:
    csv_writer = csv.writer(delta_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    line_count = 0


    # Calculate the file diff
    try:
        diff = diff_files(from_file=original_file, to_file=changed_file,
                    index_columns=[csv_index])
    except:
        print("Cannot find index column named '%s'. Exiting..." % csv_index)
        exit()

    # Write header
    csv_writer.writerow(diff['added'][0].keys())

    # loop over added rows
    for row in diff['added']:
        csv_writer.writerow(row.values())

    print("Written new records to delta.csv")

    # Loop over changed
    #change_keys = []
    #for row in diff['changed']:
    #    change_keys.append(row['key'][0])

    #with open(changed_file) as change_csv:
    #    csv_reader = csv.reader(change_csv, delimiter=',')
    #    for row in csv_reader:

    #        if(len(row) > 0 and row[0] in change_keys):
    #            csv_writer.writerow(row)
    #    print('changed records written to delta.csv')

    end_time = datetime.now()
    print('Finished.  Check delta.csv')
    print("Completed in %s seconds" % (end_time-start_time).seconds)

